#!/bin/bash -l

BASEDIR="/data/nightly"
declare -A ARCHES
ARCHES["master"]="x86_64 i586"
ARCHES["next"]="x86_64 i586 armv5tel"

MAX_AGE=$(( 10 * 24 * 3600 )) # 10 days

NOW="$(date +"%s")"

all_successful() {
	local branch="${1}"
	local release="${2}"

	local arches="${ARCHES[${branch}]}"
	[ -z "${arches}" ] && return 0

	local arch
	for arch in ${arches}; do
		if [ ! -e "${release}/${arch}/.success" ]; then
			return 1
		fi
	done

	return 0
}

for branch in $(find "${BASEDIR}" -mindepth 1 -maxdepth 1 -type d); do
	# Never remove any builds from the core branches
	case "${branch}" in
		core*)
			continue
			;;
	esac

	counter=0

	while read -r build; do
		time="$(basename "${build}")"
		[ "${time}" = "latest" ] && continue

		# Never delete the last two builds
		if [ "${counter}" -lt 2 ] && all_successful "$(basename ${branch})" "${build}"; then
			counter=$(( ${counter} + 1 ))
			continue
		fi

		# Determine age of the build
		change="$(stat --format="%Y" "${build}")"
		age=$(( ${NOW} - ${change} ))

		# If the build is old enough we will delete it
		if [[ ${age} -ge ${MAX_AGE} ]]; then
			rm -rf "${build}"
		fi
	done <<< "$(find "${branch}" -mindepth 1 -maxdepth 1 -type d | sort -nr)"

	# Delete any empty directory
	find "${branch}" -type d -empty -exec rmdir {} \; 2>/dev/null
done

exit 0
