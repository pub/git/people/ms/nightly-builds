#!/bin/bash -l

BASEDIR="/data/nightly"

main() {
	local branch
	for branch in ${BASEDIR}/*; do
		[ -d "${branch}" ] || continue

		local -A successful_releases=()

		local arch
		local release
		for release in ${branch}/*/*; do
			arch="$(basename "${release}")"

			# Remember that we have seen the architecture
			if [ -z "${successful_releases[${arch}]}" ]; then
				successful_releases["${arch}"]=""
			fi

			# Skip the "latest" symlink
			local time="$(basename "$(dirname "${release}")")"
			[ "${time}" = "latest" ] && continue

			if [ -e "${release}/.success" ]; then
				successful_releases["${arch}"]="${release}"
			fi
		done 2>/dev/null

		for arch in ${!successful_releases[@]}; do
			release="${successful_releases[${arch}]}"

			mkdir -p "${branch}/latest"
			rm -f "${branch}/latest/${arch}"

			if [ -n "${release}" ]; then
				ln -sf --relative "${release}" "${branch}/latest/${arch}"
			fi
		done
	done

	return 0
}

main || exit $?
