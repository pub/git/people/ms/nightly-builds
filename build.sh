#!/bin/bash -l

BASEDIR="/build/nightly"
LOCKFILE="/tmp/.nightly-builds.lock"

UPLOAD_DIR="${BASEDIR}/upload"
UPLOAD_TO="pakfire@fs01.haj.ipfire.org:/pub/nightly"
RSYNC_ARGS=( "-avH" "--progress" "--delay-updates" )

extract_installer_from_iso() {
	local dir="${1}"

	local tmpdir="$(mktemp -d)"
	mkdir -p "${dir}/images"

	local file
	for file in ${dir}/*.iso; do
		if mount -o loop,ro "${file}" "${tmpdir}"; then
			local f
			for f in vmlinuz instroot; do
				cp -f "${tmpdir}/boot/isolinux/${f}" "${dir}/images"
			done
			umount "${tmpdir}"

			local ext
			for ext in "" ".md5"; do
				ln -s --relative "${file}${ext}" \
					"${dir}/images/installer.iso${ext}"
			done
			break
		fi
	done 2>/dev/null

	rm -rf "${tmpdir}"
}

uriencode() {
	local path="${1}"

	local IFS="/"

	local s
	local segments=()
	for s in ${path}; do
		s="${s//'%'/%25}"
		s="${s//' '/%20}"
		s="${s//'"'/%22}"
		s="${s//'#'/%23}"
		s="${s//'$'/%24}"
		s="${s//'&'/%26}"
		s="${s//'+'/%2B}"
		s="${s//','/%2C}"
		s="${s//'/'/%2F}"
		s="${s//':'/%3A}"
		s="${s//';'/%3B}"
		s="${s//'='/%3D}"
		s="${s//'?'/%3F}"
		s="${s//'@'/%40}"
		s="${s//'['/%5B}"
		s="${s//']'/%5D}"

		segments+=( "${s}" )
	done

	echo "${segments[*]}"
}

send_email() {
	local status="${1}"
	local target="${2}"
	local branch="${3}"
	local commit="${4}"
	local build="${5}"

	sendmail -ti <<END
From: IPFire Nightly Builder <nightly-builds@ipfire.org>
To: Nightly Builds List <nightly-builds@lists.ipfire.org>
Subject: [${status^^}] Nightly Build of ${branch} (${commit:0:7}) for ${target} on ${HOSTNAME}
Date: $(date --rfc-2822)
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

https://nightly.ipfire.org$(uriencode "${build:${#UPLOAD_DIR}}")

$(git log -1 "${commit}")

https://git.ipfire.org/?p=ipfire-2.x.git;a=shortlog;h=${commit}

$(<"${build}/build.log")
END
}

build() {
	local dir="${1}"
	shift

	# Check if this actually source of IPFire 2
	[ -x "${dir}/make.sh" ] || return 1

	pushd "${dir}"

	local branch="$(git config build.branch)"
	[ -z "${branch}" ] && branch="master"

	local commit_old="$(git rev-parse HEAD)"
	git fetch

	# Search for the latest core branch
	if [ "${branch}" = "core" ]; then
		branch="$(git branch -r | awk -F/ '{ print $NF }' | grep ^core | sort --version-sort | tail -n1)"
	fi

	local commit_new="$(git rev-parse origin/${branch})"

	# If the branch was not updated, we won't build
	if [ ! -e "${dir}/.force-build" ] && [ "${commit_old}" = "${commit_new}" ]; then
		return 2
	fi

	local current_branch="$(git rev-parse --abbrev-ref HEAD)"
	if [ "${current_branch}" != "${branch}" ]; then
		git checkout -b "${branch}" "${commit_new}"
	fi

	# Checkout the latest commit
	git reset --hard "${commit_new}"

	local now="$(git log --format="%ci" -1 "${commit_new}")"

	local target
	for target in $(git config build.targets); do
		local build_path="${UPLOAD_DIR}/${branch}/${now}-${commit_new:0:8}"
		local build="${build_path}/${target}"
		local status="failed"

		# Remove marker
		unlink "${dir}/.force-build"

		# Ready for build: Download toolchain and sources
		local action
		for action in clean gettoolchain downloadsrc; do
			if ! ./make.sh --target="${target}" "${action}"; then
				touch "${dir}/.force-build"
				continue
			fi
		done

		# Execute the build
		mkdir -p "${build}"
		./make.sh --target="${target}" build | tee "${build}/build.log"
		local ret=${PIPESTATUS[0]}

		# Save the changelog
		git log -50 > "${build}/changelog.txt"

		# Save the result
		if [ "${ret}" = "0" ]; then
			status="success"
			touch "${build}/.success"

			# Copy images
			mv -v *.iso *.img.gz *.img.xz *.tar.bz2 *.md5 packages/ "${build}"
			extract_installer_from_iso "${build}"
		fi
		mv -v log/ "${build}"

		# Cleanup the build environment
		./make.sh --target="${target}" clean

		# Send an email notification
		send_email "${status}" "${target}" "${branch}" "${commit_new}" "${build}"

		# Upload the result
		sync
	done

	popd
}

sync() {
	# Do not attempt to upload anything if nothing exists
	[ ! -d "${UPLOAD_DIR}" ] && return 0

	# Acquire a Kerberos ticket for authentication
	kinit -k -t /etc/krb5.keytab "host/${HOSTNAME}"

	if rsync "${RSYNC_ARGS[@]}" "${UPLOAD_DIR}/" "${UPLOAD_TO}"; then
		rm -rf "${UPLOAD_DIR}"
	fi
}

is_locked() {
	[ -e "${LOCKFILE}" ]
}

lock() {
	touch "${LOCKFILE}"
}

unlock() {
	rm -f "${LOCKFILE}"
}

# Don't start again if the script is already running
# or if an other build script is running
if is_locked || pgrep make.sh >/dev/null; then
	exit 0
fi

# Lock
trap unlock EXIT
lock

for repo in ${BASEDIR}/*; do
	[ -d "${repo}/.git" ] || continue

	build "${repo}"
done

# Try to sync even nothing was built for retrying failed uploads
sync

exit 0
